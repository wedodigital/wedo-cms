<?php
	function wedo_posts() {

			register_post_type(
				'work',
				[
          'public' => true,
          'label' => 'Works',
          'show_in_graphql' => true,
          'supports'		=> array('title', 'editor', 'excerpt', 'revisions', 'thumbnail'),
          'graphql_single_name' => 'work',
          'graphql_plural_name' => 'works'
        ]
			);

      register_post_type(
				'sitemap',
				[
          'public' => true,
          'label' => 'Sitemaps',
          'show_in_graphql' => true,
          'graphql_single_name' => 'sitemap',
          'graphql_plural_name' => 'sitemaps'
        ]
			);

      register_post_type(
				'invoice',
				[
          'public' => true,
          'label' => 'Invoices',
          'show_in_graphql' => true,
          'graphql_single_name' => 'invoice',
          'graphql_plural_name' => 'invoices'
        ]
			);

      register_post_type(
				'guide',
				[
          'public' => true,
          'label' => 'Guides',
          'show_in_graphql' => true,
          'graphql_single_name' => 'guide',
          'graphql_plural_name' => 'guides'
        ]
			);

      register_post_type(
				'proof',
				[
          'public' => true,
          'label' => 'Proofs',
          'show_in_graphql' => true,
          'graphql_single_name' => 'proof',
          'graphql_plural_name' => 'proofs'
        ]
			);
    }
?>
